package com.rozzer.filter;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.rozzer.model.Hotel;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.rozzer.filter.FilterConstants.*;

public class FilterHotelService {


    /**
    * this code is terrible. looking at him I want to gouge out my eyes. never do that
    * */
    public static List<Hotel> filtering(List<Hotel> hotels, List<Filter> filters){
        Set<Hotel> filteredHotels = Sets.newHashSet();
        for (Filter filter: filters){
            if (filter.getName().equals(STAR) && !filter.getValues().isEmpty()) {
                filteredHotels = filterByStar(hotels, filter);
            }
            if (filter.getName().equals(FEEDBACK) && !filter.getValues().isEmpty()) {
                filteredHotels = filterByFeedback(filteredHotels, filter);
            }
            if (filter.getName().equals(PRICE) && !filter.getValues().isEmpty()) {
                filteredHotels = filterByPrice(filteredHotels, filter);
            }
            if (filter.getName().equals(OTHER) && !filter.getValues().isEmpty()) {
                filteredHotels = filterByOther(filteredHotels, filter);
            }
        }
        return Lists.newArrayList(filteredHotels);
    }

    private static Set<Hotel> filterByOther(Collection<Hotel> hotels, Filter filter) {
        Set<Hotel> filteredHotels = Sets.newHashSet();


            for (Object o : filter.getValues()) {
                if (o.toString().equals(WIFI)){
                    for (Hotel hotel : hotels) {
                        if (hotel.isWifi()){
                            filteredHotels.add(hotel);
                        }
                    }
                }
                if (o.toString().equals(BREAKFAST)){
                    for (Hotel hotel : hotels) {
                        if (hotel.isHasBreakfast()){
                            filteredHotels.add(hotel);
                        }
                    }
                }
        }
        return filteredHotels;
    }

    private static Set<Hotel> filterByPrice(Collection<Hotel> hotels, Filter filter) {
        Set<Hotel> filteredHotels = Sets.newHashSet();


            for (Object o : filter.getValues()) {
                for (Hotel hotel : hotels) {
                    if (parseAndComparison(o.toString(), hotel.getCost())){
                        filteredHotels.add(hotel);
                    }
                }
            }
        return filteredHotels;
    }

    private static Set<Hotel> filterByFeedback(Collection<Hotel> hotels, Filter filter) {
        Set<Hotel> filteredHotels = Sets.newHashSet();


            for (Object o : filter.getValues()) {
                for (Hotel hotel : hotels) {
                    if (parseAndComparison(o.toString(),  new Double(hotel.getMark()).intValue())){
                        filteredHotels.add(hotel);
                    }
                }
            }
        return filteredHotels;
    }

    private static Set<Hotel> filterByStar(Collection<Hotel> hotels, Filter filter) {
        Set<Hotel> filteredHotels = Sets.newHashSet();

            for (Object o : filter.getValues()) {
                for (Hotel hotel : hotels) {
                    if (hotel.getStars() == Integer.valueOf(o.toString())){
                        filteredHotels.add(hotel);
                    }
                }
            }
        return filteredHotels;
    }

    private static boolean parseAndComparison(String filterValue, int value) {
        String[] split = filterValue.split("-");
        return (Integer.valueOf(split[0]) >= value) || (value >= Integer.valueOf(split[1]));
    }

}
