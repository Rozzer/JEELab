package com.rozzer.filter;


import com.google.common.collect.Lists;

import java.util.List;

/*
  {"name":"Price for night", "values": ["0-3000", "3001-6000", "6001-9000", "9001-11000", "11001-15000", "15000+"]},
  {"name":"Star rating", "values": [1,2,3,4,5]},
  {"name":"Estimation by feedbacks", "values": ["9+", "8+", "7+", "6+", "No feedbacks"]},
  {"name":"Other", "values": ["Wi-Fi", "Breakfast"]}
* */

public class Filter {
    private String name;
    private List<Object> values = Lists.newArrayList();

    public Filter() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getValues() {
        return values;
    }

    public void setValues(List<Object> values) {
        this.values = values;
    }
}
