package com.rozzer.filter;

public interface FilterConstants {

    String PRICE = "Price";
    String STAR = "Star";
    String FEEDBACK = "Feedback";
    String OTHER = "Other";
    String WIFI = "Wi-Fi";
    String BREAKFAST = "Breakfast";

}
