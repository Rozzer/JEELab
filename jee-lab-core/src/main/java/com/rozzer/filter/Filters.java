package com.rozzer.filter;

import com.google.common.collect.Lists;

import java.util.List;

import static com.rozzer.filter.FilterConstants.*;

public class Filters {

    private static List<Filter> filters = Lists.newArrayList();

    static {
        Filter priceFilter = new Filter();
        priceFilter.setName(PRICE);
        priceFilter.getValues().add("0-3000");
        priceFilter.getValues().add("3001-6000");
        priceFilter.getValues().add("6001-9000");
        priceFilter.getValues().add("9001-50000");

        Filter starFilter = new Filter();
        starFilter.setName(STAR);
        starFilter.getValues().add("1");
        starFilter.getValues().add("2");
        starFilter.getValues().add("3");
        starFilter.getValues().add("4");
        starFilter.getValues().add("5");

        Filter feedbackFilter = new Filter();
        feedbackFilter.setName(FEEDBACK);
        feedbackFilter.getValues().add("1-3");
        feedbackFilter.getValues().add("4-6");
        feedbackFilter.getValues().add("7-10");

        Filter otheFilter = new Filter();
        otheFilter.setName(OTHER);
        otheFilter.getValues().add(WIFI);
        otheFilter.getValues().add(BREAKFAST);


        filters.add(priceFilter);
        filters.add(starFilter);
        filters.add(feedbackFilter);
        filters.add(otheFilter);
    }

    public static List<Filter> getFilters(){
        return filters;
    }
}
