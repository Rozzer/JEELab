package com.rozzer.model;

import com.rozzer.common.AbstractSaved;
import com.rozzer.manager.CoreServices;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "comment")
public class Comment extends AbstractSaved {

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;

    private byte rating;

    private String comment;

    public byte getRating() {
        return rating;
    }

    public void setRating(byte rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    @Override
    public void save() {
        CoreServices.serviceFor(Comment.class).save(this);
    }
}
