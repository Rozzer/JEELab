package com.rozzer.model;

import com.rozzer.common.AbstractSaved;
import com.rozzer.manager.CoreServices;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "room")
public class Room extends AbstractSaved {


    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;
    private byte space;

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public byte getSpace() {
        return space;
    }

    public void setSpace(byte space) {
        this.space = space;
    }

    @Override
    public void save() {
        CoreServices.serviceFor(Room.class).save(this);
    }
}
