package com.rozzer.model;

import com.rozzer.common.AbstractSaved;
import com.rozzer.manager.CoreServices;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "hotel")
public class Hotel extends AbstractSaved {

    @ManyToOne
    @JoinColumn(name = "place_id")
    private Place place;
    private String address;
    private int cost;
    private byte type;
    private boolean wifi;
    private boolean hasBreakfast;
    private int stars;
    private double mark;
    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;


    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public boolean isHasBreakfast() {
        return hasBreakfast;
    }

    public void setHasBreakfast(boolean hasBreakfast) {
        this.hasBreakfast = hasBreakfast;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public int getStars() { return stars; }

    public void setStars(int stars) { this.stars = stars; }

    public double getMark() { return mark;}

    public void setMark(double mark) { this.mark = mark; }


    @Override
    public void save() {
        if(Objects.isNull(this.getCity().getId())){
            if(Objects.isNull(this.getCity().getPlace().getId())){
                Place place = savePlace(this.getCity().getPlace());
                this.getCity().setPlace(place);
            }
            City city = CoreServices.serviceFor(City.class).getByName(this.getCity().getName());
            if (Objects.isNull(city)) {
                city = CoreServices.serviceFor(City.class).create();
                city.setCountry(this.getCity().getCountry());
                city.setPlace(this.getCity().getPlace());
                city.setName(this.getCity().getName());
                city.save();
            }
            this.setCity(city);
        }

        if(Objects.isNull(this.getPlace().getId())){
            Place place = savePlace(this.getPlace());
            this.setPlace(place);
        }

        CoreServices.serviceFor(Hotel.class).save(this);
    }

    private Place savePlace(Place place2) {
        Place place = CoreServices.serviceFor(Place.class).getByName(place2.getName());
        if (Objects.isNull(place)) {
            place = CoreServices.serviceFor(Place.class).create();
            place.setX(this.getCity().getPlace().getX());
            place.setY(this.getCity().getPlace().getY());
            place.setName(this.getCity().getPlace().getName());
            place.save();
        }
        return place;
    }
}
