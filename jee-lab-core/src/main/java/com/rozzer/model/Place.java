package com.rozzer.model;

import com.rozzer.common.AbstractSaved;
import com.rozzer.manager.CoreServices;

import javax.persistence.Entity;
import javax.persistence.Table;

//import javafx.util.Pair;

@Entity
@Table(name = "place")
public class Place extends AbstractSaved {

    private Double x;
    private Double y;

    /**
     * @return x, y coordinate;
     */
//    public Pair<Integer, Integer> getCoordinate(){
//        return new Pair<>(x, y);
//    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    @Override
    public void save() {
        CoreServices.serviceFor(Place.class).save(this);
    }
}
