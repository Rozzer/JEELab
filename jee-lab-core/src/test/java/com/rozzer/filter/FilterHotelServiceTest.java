package com.rozzer.filter;


import com.google.common.collect.Lists;
import com.rozzer.model.City;
import com.rozzer.model.Hotel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Random;

import static com.rozzer.filter.FilterConstants.*;


public class FilterHotelServiceTest {

    private final Random random = new Random();


    List<Filter> filters = Lists.newArrayList();
    List<Hotel> hotels = Lists.newArrayList();


    @Before
    public void generateData(){
        City city0 = createCity("Paris", "France");
        City city1 = createCity("Berlin", "German");
        City city2 = createCity("Moscow", "Russia");
        hotels.add(createNewHotel("Esprit Saint Germain",
                "22 rue Saint Sulpice, 6th arr., 75006",city0));
        hotels.add(createNewHotel("Hotel Montaigne",
                "6 Avenue Montaigne, 8th arr., 75008",city0));
        hotels.add(createNewHotel("La Tremoille Paris",
                "14 rue de la Tremoille, 8th arr., 75008",city0));
        hotels.add(createNewHotel("Le Cinq Codet",
                "5 rue Louis Codet, 7th arr., 75007 Paris, France",city0));
        hotels.add(createNewHotel("Hôtel Barrière Le Fouquet's",
                "46 Avenue George V, 8th arr., 75008",city0));
        hotels.add(createNewHotel("Sofitel Paris Le Faubourg",
                "15 Rue Boissy d'Anglas, 8th arr., 75008",city0));
        hotels.add(createNewHotel("Hôtel D'Aubusson",
                "33, Rue Dauphine, 6th arr., 75006",city0));
        hotels.add(createNewHotel("La Clef Tour Eiffel",
                "83 avenue Kléber, 16th arr., 75016",city0));
        hotels.add(createNewHotel("Hôtel Raphael",
                "17, Avenue Kléber, 16th arr., 75016",city0));

        hotels.add(createNewHotel(        "Alpha", "6 Avenue George V, 8th arr., 75008", city0,  4.2, 3));
        hotels.add(createNewHotel(        "Beta",  "6 Avenue George V, 8th arr., 75008", city0, 1.2, 2));
        hotels.add(createNewHotel(        "Gamma",  "6 Avenue George V, 8th arr., 75008", city0, 4.1, 2));
        hotels.add(createNewHotel(        "Delta",  "6 Avenue George V, 8th arr., 75008", city1, 2.2, 3));
        hotels.add(createNewHotel(        "Epsilon",  "6 Avenue George V, 8th arr., 75008", city1, 4, 3));
        hotels.add(createNewHotel(        "Zeta",  "6 Avenue George V, 8th arr., 75008", city1, 3.5, 4));
        hotels.add(createNewHotel(        "Eta",  "6 Avenue George V, 8th arr., 75008", city2, 4.8, 4));
        hotels.add(createNewHotel(        "Theta",  "6 Avenue George V, 8th arr., 75008", city2, 2.7, 4));
        hotels.add(createNewHotel(        "Iota",  "6 Avenue George V, 8th arr., 75008", city2, 3.9, 3));
        hotels.add(createNewHotel(        "Kappa",  "6 Avenue George V, 8th arr., 75008", city0, 1, 3));
        hotels.add(createNewHotel(        "Lambda",  "6 Avenue George V, 8th arr., 75008", city0, 2, 5));
        hotels.add(createNewHotel(        "Mu",  "6 Avenue George V, 8th arr., 75008", city0, 4, 2));
        hotels.add(createNewHotel(        "Nu",  "6 Avenue George V, 8th arr., 75008", city2, 3, 3));
        hotels.add(createNewHotel(        "Xi",  "6 Avenue George V, 8th arr., 75008", city2, 5, 5));
        hotels.add(createNewHotel(        "Omikron",  "6 Avenue George V, 8th arr., 75008", city2, 4.1, 5));
        hotels.add(createNewHotel(        "Pi",  "6 Avenue George V, 8th arr., 75008", city0, 4.3, 5));
        hotels.add(createNewHotel(        "Rho",  "6 Avenue George V, 8th arr., 75008", city0, 3.2, 3));
        hotels.add(createNewHotel(        "Sigma",  "6 Avenue George V, 8th arr., 75008", city0, 3.3, 3));
        hotels.add(createNewHotel(        "Tau",  "6 Avenue George V, 8th arr., 75008", city2, 3.4, 2));
        hotels.add(createNewHotel(        "Upsilon",  "6 Avenue George V, 8th arr., 75008", city2, 3.1, 3));
        hotels.add(createNewHotel(        "Phi",  "6 Avenue George V, 8th arr., 75008", city2, 1.3, 1));
        hotels.add(createNewHotel(        "Chi",  "6 Avenue George V, 8th arr., 75008", city1, 2.4, 1));
        hotels.add(createNewHotel(        "Psi",  "6 Avenue George V, 8th arr., 75008", city1, 2.3, 1));
        hotels.add(createNewHotel(        "Omega",  "6 Avenue George V, 8th arr., 75008", city1, 4.2, 2));



        Filter priceFilter = new Filter();
        priceFilter.setName(PRICE);

        Filter starFilter = new Filter();
        starFilter.setName(STAR);


        starFilter.getValues().add("3");
        starFilter.getValues().add("4");
        starFilter.getValues().add("5");

        Filter feedbackFilter = new Filter();
        feedbackFilter.setName(FEEDBACK);
        feedbackFilter.getValues().add("4-6");
        feedbackFilter.getValues().add("7-10");

        Filter otheFilter = new Filter();
        otheFilter.setName(OTHER);
        otheFilter.getValues().add(WIFI);

        filters.add(priceFilter);
        filters.add(starFilter);
        filters.add(feedbackFilter);
        filters.add(otheFilter);
    }

    private Hotel createNewHotel(String name, String address, City city, double mark, int stars) {
        Hotel hotel = new Hotel();
        hotel.setName(name);
        hotel.setAddress(address);
        hotel.setCity(city);
        hotel.setCost(random.nextInt(50000));
        hotel.setType((byte) 0);
        hotel.setStars((byte) random.nextInt(5));
        hotel.setStars(stars);
        hotel.setMark(mark);
        hotel.setWifi(random.nextBoolean());
        hotel.setHasBreakfast(random.nextBoolean());

        return hotel;
    }

    private Hotel createNewHotel(String name, String address,  City city) {
        Hotel hotel = new Hotel();
        hotel.setName(name);
        hotel.setAddress(address);
        hotel.setCity(city);
        hotel.setCost(random.nextInt(50000));
        hotel.setType((byte) 0);
        hotel.setStars((byte) random.nextInt(5));
        hotel.setWifi(random.nextBoolean());
        hotel.setHasBreakfast(random.nextBoolean());

        return hotel;
    }

    public City createCity(String name, String country) {
        City city = new City();
        city.setName(name);
        city.setCountry(country);
        return city;
    }



    @Test
    public void filteringTest() {
        List<Hotel> filtering = FilterHotelService.filtering(hotels, filters);
        Assert.assertTrue(filtering.size() != 0);
        Assert.assertTrue(filtering.size() < hotels.size());
    }



    @Test
    public void test() {
        String[] split = "1-3".split("-");
        Assert.assertEquals("1", split[0]);
        Assert.assertEquals("3", split[1]);
    }
}
