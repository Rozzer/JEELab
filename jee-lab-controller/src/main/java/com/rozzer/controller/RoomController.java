package com.rozzer.controller;

import com.rozzer.controller.common.Controller;
import com.rozzer.model.Room;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.rozzer.controller.common.ControllerHelper.service;

@RestController
@RequestMapping(value = "/api/room")
public class RoomController implements Controller<Room> {

    @Override
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Room> getAll() {
        return service(Room.class).getAll();
    }


    @RequestMapping(value = "/allbyparent")
    public List<Room> getAllByParent(@RequestParam(value = "id") String parentId){
        return service(Room.class).getAllByParent(new Long(parentId));
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public Room create() {
        return service(Room.class).create();
    }

    @Override
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Room read(@RequestParam(value = "id") String id) {
        return service(Room.class).getById(new Long(id));
    }

    @Override
    @RequestMapping(method = RequestMethod.PUT)
    public void update(@RequestBody Room object) {
        service(Room.class).save(object);
    }

    @Override
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@RequestBody Room object) {
        service(Room.class).delete(object);
    }
}
