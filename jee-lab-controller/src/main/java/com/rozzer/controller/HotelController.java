package com.rozzer.controller;

import com.google.common.collect.Lists;
import com.rozzer.controller.common.CollectionAndTotalPage;
import com.rozzer.controller.common.Controller;
import com.rozzer.filter.Filter;
import com.rozzer.filter.FilterHotelService;
import com.rozzer.filter.Filters;
import com.rozzer.integration.hotels.model.Characteristic;
import com.rozzer.integration.maps.service.IntegrationService;
import com.rozzer.model.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.rozzer.controller.common.ControllerHelper.service;

@RestController
@RequestMapping(value = "/api/hotel")
public class HotelController implements Controller<Hotel> {
    @Autowired
    private IntegrationService integration;

    @Override
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Hotel> getAll() {
        return service(Hotel.class).getAll();
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public Hotel create() {
        return service(Hotel.class).create();
    }

    @Override
//    @RequestMapping(method = RequestMethod.GET)
    public Hotel read(@RequestParam(value = "id") String id) {
        return service(Hotel.class).getById(new Long(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Characteristic> getInfoById(@RequestParam(value = "id") String id){  // return Characs or String ?
        return integration.getInfo(service(Hotel.class).getById(new Long(id)));
    }


    @Override
    @RequestMapping(method = RequestMethod.PUT)
    public void update(@RequestBody Hotel object) {
        service(Hotel.class).save(object);
    }

    @Override
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@RequestBody Hotel object) {
        service(Hotel.class).delete(object);
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public List<Hotel> getPage(@RequestParam(value = "page") String page){
        return service(Hotel.class).getPage(Integer.valueOf(page));
    }


    @RequestMapping(value = "/pagewithtotal", method = RequestMethod.GET)
    public CollectionAndTotalPage getHotelsWithTotalPage(@RequestParam(value = "page") String page){
        CollectionAndTotalPage collectionAndTotalPage = new CollectionAndTotalPage();
        collectionAndTotalPage.setHotels(service(Hotel.class).getPage(Integer.valueOf(page)));
        collectionAndTotalPage.setTotalpages(service(Hotel.class).getTotalPage());
        return collectionAndTotalPage;
    }

    @RequestMapping(value = "/totalpage", method = RequestMethod.GET)
    public int getTotalPage(){
        return service(Hotel.class).getTotalPage();
    }

    @RequestMapping(value = "/city", method = RequestMethod.GET)
    public List<Hotel> getHotelsByCity(@RequestParam(value = "city") String city){
        return service(Hotel.class).getAllByParent(new Long(city));
    }

    @RequestMapping(value = "/filters", method = RequestMethod.GET)
    public List<Filter> getAllFiltersForHotel(){

        return Filters.getFilters();
    }

    @RequestMapping(value = "/filter", method = RequestMethod.POST)
    public List<Hotel> getHotelByFilter(@RequestBody List<Filter> filters){
        List<Hotel> hotels = service(Hotel.class).getAll();
        return hotels;
    }

    @RequestMapping(value = "/filterwithtotal", method = RequestMethod.POST)
    public CollectionAndTotalPage getHotelByFilterWithPage(@RequestParam(value = "page") String page, @RequestBody List<Filter> filters){
        if (filters.isEmpty()){
            CollectionAndTotalPage collectionAndTotalPage = new CollectionAndTotalPage();
            collectionAndTotalPage.setHotels(service(Hotel.class).getAll());
            collectionAndTotalPage.setTotalpages(service(Hotel.class).getTotalPage());
            return collectionAndTotalPage;
        }
        List<Hotel> hotels = FilterHotelService.filtering(service(Hotel.class).getAll(), filters);
        int i = hotels.size() / 10;
        hotels = getOnlyPageFilterHotels(i, page, hotels);
        CollectionAndTotalPage collectionAndTotalPage = new CollectionAndTotalPage();
        collectionAndTotalPage.setHotels(hotels);
        collectionAndTotalPage.setTotalpages(i);
        return collectionAndTotalPage;
    }

    private List<Hotel> getOnlyPageFilterHotels(int i, String page, List<Hotel> hotels) {
        if (Integer.valueOf(page) > i){
            throw new RuntimeException();
        }
        List<Hotel> hotelPage = Lists.newArrayList();
        int index = i * 10;
        int sizeHotels = hotels.size();
        int maxHotelIndex = index + 10;
        for (int j = index; j < sizeHotels && j < maxHotelIndex; j++) {
            hotelPage.add(hotels.get(j));
        }

        return hotelPage;
    }
}
