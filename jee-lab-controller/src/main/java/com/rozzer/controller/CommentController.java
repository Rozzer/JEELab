package com.rozzer.controller;

import com.rozzer.controller.common.Controller;
import com.rozzer.model.Comment;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.rozzer.controller.common.ControllerHelper.service;

@RestController
@RequestMapping(value = "/api/comemet")
public class CommentController implements Controller<Comment> {

    @Override
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Comment> getAll() {
        return service(Comment.class).getAll();
    }

    @RequestMapping(value = "/allbyparent")
    public List<Comment> getAllByParent(@RequestParam(value = "id") String parentId){
        return service(Comment.class).getAllByParent(new Long(parentId));
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public Comment create() {
        return service(Comment.class).create();
    }

    @Override
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Comment read(@RequestParam(value = "id") String id) {
        return service(Comment.class).getById(new Long(id));
    }

    @Override
    @RequestMapping(method = RequestMethod.PUT)
    public void update(@RequestBody Comment object) {
        service(Comment.class).save(object);
    }

    @Override
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@RequestBody Comment object) {
        service(Comment.class).delete(object);
    }


    @RequestMapping(method = RequestMethod.GET)
    public List<Comment> showPage (@RequestParam(defaultValue = "0", value = "page") int page){
        return service(Comment.class).getPage(page);
    }

}
