package com.rozzer.controller.common;

import com.rozzer.model.Hotel;

import java.util.List;

public class CollectionAndTotalPage {
    private int totalpages;
    private List<Hotel>  hotels;

    public int getTotalpages() {
        return totalpages;
    }

    public void setTotalpages(int totalpages) {
        this.totalpages = totalpages;
    }

    public List<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
    }
}
