package com.rozzer.controller;

import com.rozzer.controller.common.Controller;
import com.rozzer.model.City;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.rozzer.controller.common.ControllerHelper.service;

@RestController
@RequestMapping(value = "/api/city")
public class CityController implements Controller<City> {

    @Override
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<City> getAll() {
        return service(City.class).getAll();
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public City create() {
        return service(City.class).create();
    }

    @Override
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public City read(@RequestParam(value = "id") String id) {
        return service(City.class).getById(new Long(id));
    }

    @Override
    @RequestMapping(method = RequestMethod.PUT)
    public void update(@RequestBody City object) {
        service(City.class).save(object);
    }

    @Override
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@RequestBody City object) {
        service(City.class).delete(object);
    }


    @RequestMapping(method = RequestMethod.GET)
    public List<City> showPage (@RequestParam(defaultValue = "0", value = "page") int page){
        return service(City.class).getPage(page);
    }

}
