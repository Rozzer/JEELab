package com.rozzer.spring.services;

import com.rozzer.manager.CoreServices;
import com.rozzer.manager.EntityService;
import com.rozzer.model.Comment;
import com.rozzer.model.Hotel;
import com.rozzer.spring.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

public class CommentService implements EntityService<Comment> {

    private CommentRepository commentRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
        CoreServices.getServiceFactory().register(Hotel.class, this);
    }

    @Override
    public List<Comment> getAll() {
        throw new NotImplementedException();
    }

    @Override
    public void save(Comment saved) {
        throw new NotImplementedException();
    }

    @Override
    public void delete(Comment saved) {
        throw new NotImplementedException();
    }

    @Override
    public Comment getById(Long id) {
        throw new NotImplementedException();
    }

    @Override
    public Comment getByName(String name) {
        throw new NotImplementedException();
    }

    @Override
    public Comment create() {
        throw new NotImplementedException();
    }

    @Override
    public List<Comment> getPage(int page) {
        throw new NotImplementedException();
    }

    @Override
    public int getTotalPage() {
        throw new NotImplementedException();
    }

    @Override
    public List<Comment> getAllByParent(Long parentId) {
        throw new NotImplementedException();
    }
}
