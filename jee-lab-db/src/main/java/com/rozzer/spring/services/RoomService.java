package com.rozzer.spring.services;

import com.rozzer.manager.CoreServices;
import com.rozzer.manager.EntityService;
import com.rozzer.model.Room;
import com.rozzer.spring.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

public class RoomService implements EntityService<Room> {

    private RoomRepository roomRepository;

    @Autowired
    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
        CoreServices.getServiceFactory().register(Room.class, this);
    }

    @Override
    public List<Room> getAll() {
        throw new NotImplementedException();
    }

    @Override
    public void save(Room saved) {
        throw new NotImplementedException();
    }

    @Override
    public void delete(Room saved) {
        throw new NotImplementedException();
    }

    @Override
    public Room getById(Long id) {
        throw new NotImplementedException();
    }

    @Override
    public Room getByName(String name) {
        throw new NotImplementedException();
    }

    @Override
    public Room create() {
        throw new NotImplementedException();
    }

    @Override
    public List<Room> getPage(int page) {
        throw new NotImplementedException();
    }

    @Override
    public int getTotalPage() {
        throw new NotImplementedException();
    }

    @Override
    public List<Room> getAllByParent(Long parentId) {
        throw new NotImplementedException();
    }
}
