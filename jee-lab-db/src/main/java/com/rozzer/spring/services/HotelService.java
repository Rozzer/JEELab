package com.rozzer.spring.services;

import com.google.common.collect.Lists;
import com.rozzer.manager.CoreServices;
import com.rozzer.manager.EntityService;
import com.rozzer.model.City;
import com.rozzer.model.Hotel;
import com.rozzer.spring.repositories.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotelService implements EntityService<Hotel> {

    private HotelRepository hotelRepository;

    @Autowired
    public HotelService(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
        CoreServices.getServiceFactory().register(Hotel.class, this);
    }

    @Override
    public List<Hotel> getAll() {
        return Lists.newArrayList(hotelRepository.findAll());
    }

    @Override
    public void save(Hotel saved) {
        hotelRepository.save(saved);
    }

    @Override
    public void delete(Hotel saved) {
        hotelRepository.delete(saved);
    }

    @Override
    public Hotel getById(Long id) {
        return hotelRepository.findById(id).get();
    }

    @Override
    public Hotel getByName(String name) {
        if (hotelRepository.getByName(name).isEmpty()){
            return null;
        }
        return hotelRepository.getByName(name).stream().findFirst().get();
    }

    @Override
    public Hotel create() {
        Hotel author = new Hotel();
        return hotelRepository.save(author);
    }

    @Override
    public List<Hotel> getPage(int page) {
        return Lists.newArrayList(hotelRepository.findAll(PageRequest.of(page, 10)));
    }

    @Override
    public int getTotalPage() {
        return hotelRepository.findAll(PageRequest.of(1, 10)).getTotalPages();
    }

    @Override
    public List<Hotel> getAllByParent(Long parentId) {
        return Lists.newArrayList(hotelRepository.getAllByCity(
                (City) CoreServices.getServiceFactory().service(City.class).getById(parentId))
        );
    }
}
