package com.rozzer.spring.services;

import com.google.common.collect.Lists;
import com.rozzer.manager.CoreServices;
import com.rozzer.manager.EntityService;
import com.rozzer.model.City;
import com.rozzer.spring.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

@Service
public class CityService implements EntityService<City> {

    private CityRepository cityRepository;

    @Autowired
    public CityService(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
        CoreServices.getServiceFactory().register(City.class, this);
    }

    public List<City> getAll() {
        return Lists.newArrayList(cityRepository.findAll());
    }

    public void save(City saved) {
        cityRepository.save(saved);
    }

    public void delete(City saved) {
        cityRepository.delete(saved);
    }

    public City getById(Long id) {
        return cityRepository.findById(id).get();
    }

    @Override
    public City getByName(String name) {
        if (cityRepository.getByName(name).isEmpty()){
            return null;
        }
        return cityRepository.getByName(name).stream().findFirst().get();
    }

    public City create() {
        City city = new City();
        return cityRepository.save(city);
    }

    public List<City> getPage(int page){
        Page<City> all = cityRepository.findAll(PageRequest.of(page, 10));
        return all.getContent();
    }

    @Override
    public int getTotalPage() {
        throw new NotImplementedException();
    }

    @Override
    public List<City> getAllByParent(Long parentId) {
        throw  new NotImplementedException();
    }
}
