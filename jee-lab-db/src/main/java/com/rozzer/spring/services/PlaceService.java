package com.rozzer.spring.services;

import com.rozzer.manager.CoreServices;
import com.rozzer.manager.EntityService;
import com.rozzer.model.Place;
import com.rozzer.spring.repositories.PlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

@Service
public class PlaceService implements EntityService<Place> {


    private PlaceRepository placeRepository;

    @Autowired
    public PlaceService(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
        CoreServices.getServiceFactory().register(Place.class, this);
    }

    @Override
    public List<Place> getAll() {
        return placeRepository.findAll();
    }

    @Override
    public void save(Place saved) {
        placeRepository.save(saved);
    }

    @Override
    public void delete(Place saved) {
        placeRepository.delete(saved);
    }

    @Override
    public Place getById(Long id) {
        return placeRepository.getOne(id);
    }

    @Override
    public Place getByName(String name) {
        if (placeRepository.getByName(name).isEmpty()){
            return null;
        }
        return placeRepository.getByName(name).stream().findFirst().get();
    }

    @Override
    public Place create() {
        Place place = new Place();
        return placeRepository.save(place);
    }

    @Override
    public List<Place> getPage(int page) {
        throw new NotImplementedException();
    }

    @Override
    public int getTotalPage() {
        throw new NotImplementedException();
    }

    @Override
    public List<Place> getAllByParent(Long parentId) {
        throw new NotImplementedException();
    }
}
