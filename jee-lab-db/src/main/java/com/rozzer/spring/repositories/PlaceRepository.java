package com.rozzer.spring.repositories;

import com.rozzer.model.Place;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlaceRepository extends JpaRepository<Place, Long> {
    List<Place> getByName(String name);

}
