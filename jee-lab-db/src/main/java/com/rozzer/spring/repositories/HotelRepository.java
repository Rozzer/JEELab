package com.rozzer.spring.repositories;

import com.rozzer.model.City;
import com.rozzer.model.Hotel;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface HotelRepository extends PagingAndSortingRepository<Hotel, Long> {
    List<Hotel> getAllByCity(City city);
    List<Hotel> getByName(String name);

}
