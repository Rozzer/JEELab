package com.rozzer.spring.repositories;

import com.rozzer.model.Hotel;
import com.rozzer.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface RoomRepository extends JpaRepository<Room, Long> {

    Collection<Room> getAllByHotel(Hotel hotel);
}
