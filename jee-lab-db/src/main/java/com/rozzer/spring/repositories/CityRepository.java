package com.rozzer.spring.repositories;

import com.rozzer.model.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityRepository extends JpaRepository<City, Long> {

    List<City> getByName(String name);
}
