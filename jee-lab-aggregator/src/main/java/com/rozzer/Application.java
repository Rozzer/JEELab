package com.rozzer;

import com.rozzer.integration.maps.controller.ExternalController;
import com.rozzer.manager.CoreServices;
import com.rozzer.model.City;
import com.rozzer.model.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.Random;


@SuppressWarnings("SpringFacetCodeInspection")
@SpringBootApplication
public class Application {


    @Autowired
    private ExternalController externalController;

    public static void main(String[] args) {
        CoreServices.setServiceFactory(new DBServiceFactory());
        SpringApplication.run(Application.class, args);
    }

    /**
     * write all beans in console.
     * @param ctx spring context
     * @return bean
     */
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }

//            generateData();

//            genCity();//WA
            externalController.getAll().forEach(Hotel::save);
        };
    }

    private final Random random = new Random();


    @Deprecated
    private void genCity(){
       createCity("Paris", "France");
        createCity("Berlin", "German");
        createCity("Moscow", "Russia");
    }


    @Deprecated
    private void generateData(){
        City city0 = createCity("Paris", "France");
        City city1 = createCity("Berlin", "German");
        City city2 = createCity("Moscow", "Russia");
        createNewHotel("Esprit Saint Germain",
                "22 rue Saint Sulpice, 6th arr., 75006",city0);
        createNewHotel("Hotel Montaigne",
                "6 Avenue Montaigne, 8th arr., 75008",city0);
        createNewHotel("La Tremoille Paris",
                "14 rue de la Tremoille, 8th arr., 75008",city0);
        createNewHotel("Le Cinq Codet",
                "5 rue Louis Codet, 7th arr., 75007 Paris, France",city0);
        createNewHotel("Hôtel Barrière Le Fouquet's",
                "46 Avenue George V, 8th arr., 75008",city0);
        createNewHotel("Sofitel Paris Le Faubourg",
                "15 Rue Boissy d'Anglas, 8th arr., 75008",city0);
        createNewHotel("Hôtel D'Aubusson",
                "33, Rue Dauphine, 6th arr., 75006",city0);
        createNewHotel("La Clef Tour Eiffel",
                "83 avenue Kléber, 16th arr., 75016",city0);
        createNewHotel("Hôtel Raphael",
                "17, Avenue Kléber, 16th arr., 75016",city0);

        createNewHotel(        "Alpha", "6 Avenue George V, 8th arr., 75008", city0,  4.2, 3);
        createNewHotel(        "Beta",  "6 Avenue George V, 8th arr., 75008", city0, 1.2, 2);
        createNewHotel(        "Gamma",  "6 Avenue George V, 8th arr., 75008", city0, 4.1, 2);
        createNewHotel(        "Delta",  "6 Avenue George V, 8th arr., 75008", city1, 2.2, 3);
        createNewHotel(        "Epsilon",  "6 Avenue George V, 8th arr., 75008", city1, 4, 3);
        createNewHotel(        "Zeta",  "6 Avenue George V, 8th arr., 75008", city1, 3.5, 4);
        createNewHotel(        "Eta",  "6 Avenue George V, 8th arr., 75008", city2, 4.8, 4);
        createNewHotel(        "Theta",  "6 Avenue George V, 8th arr., 75008", city2, 2.7, 4);
        createNewHotel(        "Iota",  "6 Avenue George V, 8th arr., 75008", city2, 3.9, 3);
        createNewHotel(        "Kappa",  "6 Avenue George V, 8th arr., 75008", city0, 1, 3);
        createNewHotel(        "Lambda",  "6 Avenue George V, 8th arr., 75008", city0, 2, 5);
        createNewHotel(        "Mu",  "6 Avenue George V, 8th arr., 75008", city0, 4, 2);
        createNewHotel(        "Nu",  "6 Avenue George V, 8th arr., 75008", city2, 3, 3);
        createNewHotel(        "Xi",  "6 Avenue George V, 8th arr., 75008", city2, 5, 5);
        createNewHotel(        "Omikron",  "6 Avenue George V, 8th arr., 75008", city2, 4.1, 5);
        createNewHotel(        "Pi",  "6 Avenue George V, 8th arr., 75008", city0, 4.3, 5);
        createNewHotel(        "Rho",  "6 Avenue George V, 8th arr., 75008", city0, 3.2, 3);
        createNewHotel(        "Sigma",  "6 Avenue George V, 8th arr., 75008", city0, 3.3, 3);
        createNewHotel(        "Tau",  "6 Avenue George V, 8th arr., 75008", city2, 3.4, 2);
        createNewHotel(        "Upsilon",  "6 Avenue George V, 8th arr., 75008", city2, 3.1, 3);
        createNewHotel(        "Phi",  "6 Avenue George V, 8th arr., 75008", city2, 1.3, 1);
        createNewHotel(        "Chi",  "6 Avenue George V, 8th arr., 75008", city1, 2.4, 1);
        createNewHotel(        "Psi",  "6 Avenue George V, 8th arr., 75008", city1, 2.3, 1);
        createNewHotel(        "Omega",  "6 Avenue George V, 8th arr., 75008", city1, 4.2, 2);
    }

    private Hotel createNewHotel(String name, String address,  City city, double mark, int stars) {
        Hotel hotel = CoreServices.serviceFor(Hotel.class).create();
        hotel.setName(name);
        hotel.setAddress(address);
        hotel.setCity(city);
        hotel.setCost(random.nextInt(50000));
        hotel.setType((byte) 0);
        hotel.setStars((byte) random.nextInt(5));
        hotel.setStars(stars);
        hotel.setMark(mark);
        hotel.setWifi(random.nextBoolean());
        hotel.setHasBreakfast(random.nextBoolean());
        hotel.save();

        return hotel;
    }

    private Hotel createNewHotel(String name, String address,  City city) {
        Hotel hotel = CoreServices.serviceFor(Hotel.class).create();
        hotel.setName(name);
        hotel.setAddress(address);
        hotel.setCity(city);
        hotel.setCost(random.nextInt(50000));
        hotel.setType((byte) 0);
        hotel.setStars((byte) random.nextInt(5));
        hotel.setWifi(random.nextBoolean());
        hotel.setHasBreakfast(random.nextBoolean());
        hotel.save();

        return hotel;
    }

    public City createCity(String name, String country) {
        City city = CoreServices.serviceFor(City.class).create();
        city.setName(name);
        city.setCountry(country);
        city.save();
        return city;
    }


}
