package com.rozzer.integration.hotels.model;

public class ExAddress {
    private String placeid;
    private String address;
    private double latitude;
    private double longitude;
    private ExCity city;

    public ExAddress() {
    }

    public ExAddress(String placeid, String address, double latitude, double longitude, ExCity city) {
        this.placeid = placeid;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
    }

    public String getPlaceid() {
        return placeid;
    }

    public void setPlaceid(String placeid) {
        this.placeid = placeid;
    }

    public ExCity getCity() {
        return city;
    }

    public void setCity(ExCity city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
