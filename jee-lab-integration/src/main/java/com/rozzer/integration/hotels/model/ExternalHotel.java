package com.rozzer.integration.hotels.model;

import java.util.List;

/**
 * {
 * "название":"", //основное
 * "средняя цена":"", //основное
 * "количество свободных номеров":"", // основное
 * "адрес":"", // основное
 * "фото":"", ??? // хз хз
 * "характеристики":[{},{}....{}], // запехнем все шо придумает, например: номер телефона, адрес, сайт и особенности и тд...
 * }
 */
public class ExternalHotel {
    private String name;
    private Integer cost;
    private Short freeRoom;
    private ExAddress address;
    private List<Characteristic> characteristics;

    public ExternalHotel() {
    }

    public List<Characteristic> getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(List<Characteristic> characteristics) {
        this.characteristics = characteristics;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Short getFreeRoom() {
        return freeRoom;
    }

    public void setFreeRoom(Short freeRoom) {
        this.freeRoom = freeRoom;
    }

    public ExAddress getAddress() {
        return address;
    }

    public void setAddress(ExAddress address) {
        this.address = address;
    }
}
