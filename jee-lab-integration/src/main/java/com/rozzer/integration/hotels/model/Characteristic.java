package com.rozzer.integration.hotels.model;

import java.util.List;


public class Characteristic {
    private String name;
    private String value;
    private List<String> characs;

    public Characteristic() {
    }

    public Characteristic(String name, String value, List<String> characs) {
        this.name = name;
        this.value = value;
        this.characs = characs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<String> getCharacs() {
        return characs;
    }

    public void setCharacs(List<String> characs) {
        this.characs = characs;
    }

    public void addCharacteristic(String characteristic){characs.add(characteristic);}
}
