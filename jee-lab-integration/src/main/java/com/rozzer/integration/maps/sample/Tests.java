package com.rozzer.integration.maps.sample;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import com.rozzer.integration.hotels.model.Characteristic;
import com.rozzer.integration.maps.service.BookingService;

import java.io.IOException;
import java.util.List;

public class Tests {
    public static final String API_KEY = "AIzaSyAbRfNJ6xXGUpESXYhq_gZVb6TdNYkhfow";



    public static void main(String[] args) throws InterruptedException, ApiException, IOException {
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey(API_KEY)
                .build();
        Gson json = new GsonBuilder().setPrettyPrinting().create();
        BookingService booking = new BookingService();
        LatLng coord = new LatLng(48.8357962,2.3513034);
        List<Characteristic> chars = booking.getInfo(coord);
        System.out.println(chars.toString());

    }
}