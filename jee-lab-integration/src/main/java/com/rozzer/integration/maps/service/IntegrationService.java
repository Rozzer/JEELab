package com.rozzer.integration.maps.service;

import com.google.maps.model.LatLng;
import com.rozzer.integration.hotels.model.Characteristic;
import com.rozzer.model.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IntegrationService {

    @Autowired
    private BookingService booking;


    public List<Characteristic> getInfo(Hotel hotel) {

        double lng = hotel.getPlace().getX();
        double lat = hotel.getPlace().getY();

        LatLng coordinates = new LatLng(lat,lng);

        return booking.getInfo(coordinates);
    }
}
