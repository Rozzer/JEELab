package com.rozzer.integration.maps.controller;

import com.rozzer.integration.hotels.model.ExternalHotel;
import com.rozzer.integration.maps.service.BookingService;
import com.rozzer.model.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ExternalController {

    @Autowired
    private BookingService booking;

    public List<Hotel> getAll(){
        return booking.getAll();
    }


}
