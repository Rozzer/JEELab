package com.rozzer.integration.maps.utils;

public class Filters {
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "lng";
    public static final String NEXT_PAGE = "nextPage";
    /*
    * достопримечательности: SHOWPLACE + ESTABLISHMENT => Колизей и т.д.
    * знаковые места: PARK + SHOWPLACE [+ESTABLISHMENT] => Гранд-Каньон и т.д.
    * естественные особенности: NATURAL + SHOWPLACE + ESTABLISHMENT => Ниагарский водопад и т.д.
    * */
    // достопримечательности и знаковые места
    public static final String PARK = "park";
    public static final String NATURAL = "natural_feature";
    public static final String SHOWPLACE = "point_of_interest";
    public static final String ESTABLISHMENT = "establishment";
}
