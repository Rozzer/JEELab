package com.rozzer.integration.maps.dto;

import com.rozzer.model.Hotel;

import java.util.List;
import java.util.Map;

public class FilterRequest {
    private List<String> filters;
    private Map<String,String> kvFilters;
    private List<Hotel> hotels;

    public FilterRequest() {
    }

    public List<String> getFilters() {
        return filters;
    }

    public void setFilters(List<String> filters) {
        this.filters = filters;
    }

    public Map<String, String> getKvFilters() {
        return kvFilters;
    }

    public void setKvFilters(Map<String, String> kvFilters) {
        this.kvFilters = kvFilters;
    }

    public List<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
    }
}
