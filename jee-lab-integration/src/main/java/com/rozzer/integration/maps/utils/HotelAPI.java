package com.rozzer.integration.maps.utils;

public class HotelAPI {
    public static final String HOST = "https://hotel-stub.herokuapp.com";
    public static final String HOTEL = "/hotel";
    public static final String GET_BY_PLACE_ID = "/placeids";

    public static final String GET_ALL = "/all";
    public static final String GET_INFO = "/info";
}
