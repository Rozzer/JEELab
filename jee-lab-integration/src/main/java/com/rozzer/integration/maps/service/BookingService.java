package com.rozzer.integration.maps.service;

import com.google.maps.model.LatLng;
import com.rozzer.integration.hotels.model.Characteristic;
import com.rozzer.integration.hotels.model.ExAddress;
import com.rozzer.integration.hotels.model.ExternalHotel;
import com.rozzer.integration.maps.utils.HotelAPI;
import com.rozzer.model.City;
import com.rozzer.model.Hotel;
import com.rozzer.model.Place;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class BookingService {

    private String url = HotelAPI.HOST+HotelAPI.HOTEL;


    @Autowired
    private RestTemplate template;

    public List<Hotel> getAll() {
        ExternalHotel[] bookings = template.getForObject(url+HotelAPI.GET_ALL,ExternalHotel[].class);
        List<Hotel> hotels = toHotel(bookings);
        return hotels;
    }

    public List<Characteristic> getInfo(LatLng coordinates) {
        Map<String,Double> params = new HashMap<>();
        params.put("lat",coordinates.lat);
        params.put("lng",coordinates.lng);

        ExternalHotel[] resp = template.getForObject(url+HotelAPI.GET_INFO,ExternalHotel[].class,params);

        if (resp.length>1) {
             return Arrays.stream(resp).collect(
                   ArrayList::new,
                   (list,item) -> list.addAll(item.getCharacteristics()),
                   ArrayList::addAll);
        }
        else return resp[0].getCharacteristics();

    }

    // TODO: for map markers
    public void getHotelsPrices(List<String> placeIDs){

    }

    private List<Hotel> toHotel(ExternalHotel[] bookings) {
        List<Hotel> hotels = new ArrayList<>(bookings.length);

        Arrays.stream(bookings).forEach(b->{
            ExAddress adr = b.getAddress();
            Place hPlace = new Place();
            hPlace.setX(adr.getLongitude());
            hPlace.setY(adr.getLatitude());
            hPlace.setName(adr.getPlaceid());

            Place cPlace = new Place();
            cPlace.setY(adr.getCity().getLatitude());
            cPlace.setX(adr.getCity().getLongitude());
            cPlace.setName(adr.getCity().getName());

            City city = new City();
            city.setPlace(cPlace);
            city.setName(adr.getCity().getName());
            city.setCountry(adr.getCity().getCountry());

            Hotel tmp = new Hotel();
            tmp.setAddress(adr.getAddress());
            tmp.setCity(city);
            tmp.setPlace(hPlace);

            tmp.setName(b.getName());
            tmp.setCost(b.getCost());

            extractCharacteristic(b,tmp);

            hotels.add(tmp);
        });

        return hotels;
    }

    private void extractCharacteristic(ExternalHotel booking, Hotel hotel){
        booking.getCharacteristics().stream()
                .filter(c->Objects.nonNull(c.getName()) && Objects.nonNull(c.getValue())
                        && c.getName().equalsIgnoreCase("facilities") && c.getValue().equalsIgnoreCase("in_room"))
                .findFirst()
                .ifPresent(chr->{
                    hotel.setWifi(chr.getCharacs().contains("wifi"));
                    hotel.setHasBreakfast(chr.getCharacs().contains("breakfast"));
                });

        booking.getCharacteristics().stream().filter(c->Objects.nonNull(c.getName()) && c.getName().equalsIgnoreCase("stars"))
                .findFirst()
                .ifPresent(ch->hotel.setStars(Integer.valueOf(ch.getValue())));

//        booking.getCharacteristics().stream().filter(c->c.getName().equalsIgnoreCase("type")).findFirst().ifPresent(ch->hotel.setType(ch.getValue()));

    }
}
