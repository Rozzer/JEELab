package com.rozzer.integration.maps.controller;


import com.google.maps.errors.ApiException;
import com.rozzer.integration.maps.dto.FilterRequest;
import com.rozzer.integration.maps.dto.PlaceRequest;
import com.rozzer.integration.maps.dto.PlaceResponse;
import com.rozzer.integration.maps.service.MapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/*



* Поиск места с помощью набора фильтров/параметров:
      * Удалённость до места,                                       distance
      * время в пути,                                               on travel time
      * наличие в ближайшей доступности воды (моря, океаны, реки),  water availability
      * архитектурных достопримечательностей,                       architectural sights
      * знаковых мест.                                              sign places
* Предусмотреть возможность поиска места по видам активного отдыха и досуга.

*/
@RestController
@RequestMapping(value = "/map")
public class MapController {

    @Autowired
    private MapService service;

    @RequestMapping
    public String test(){
        return "Map Controller is avaliable";
    }

    @CrossOrigin(origins = "https://guarded-shore-44262.herokuapp.com")
    @RequestMapping(value = "/find/places", method = RequestMethod.POST, consumes = {"application/json"})
    public List<PlaceResponse> getHotelsAroundPlace(@RequestBody PlaceRequest request) throws InterruptedException, ApiException, IOException {
        return service.getHotelsAroundPlace(request);
    }

    @RequestMapping(value = "/filter")
    public FilterRequest filter(FilterRequest filter){

        return null;
    }

    @RequestMapping(value = "/filter/showplaces")
    public FilterRequest filterByShowplaces(FilterRequest filter) throws InterruptedException, ApiException, IOException {
        if (filter.getFilters().isEmpty()) return null;
        filter = service.getByShowplace(filter); // get what ???? hotels ???
        return filter;
    }

    @RequestMapping(value = "/filter/distance")
    public FilterRequest filterByDistance(FilterRequest filter){

        return null;
    }


    @RequestMapping(value = "/filter/travel-time")
    public FilterRequest filterByTravelTime(FilterRequest filter){

        return null;
    }

    @RequestMapping(value = "/filter/water-availability")
    public FilterRequest filterByWaterAvailability(FilterRequest filter){

        return null;

    }

    // получить отели в радиусе 10000 относительно точки
    @RequestMapping(value = "/filter/circle", method = RequestMethod.POST)
    public FilterRequest filterByRadius(FilterRequest filter) throws InterruptedException, ApiException, IOException {
        if (filter.getKvFilters().isEmpty()) return null;

        filter = service.getWithinRadios(filter);
        return filter;
    }

}
