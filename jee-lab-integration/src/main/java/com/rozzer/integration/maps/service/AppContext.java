package com.rozzer.integration.maps.service;

import com.google.maps.GeoApiContext;

public class AppContext {
    private static final String API_KEY = "AIzaSyAbRfNJ6xXGUpESXYhq_gZVb6TdNYkhfow";

    private static final GeoApiContext context = new GeoApiContext.Builder()
            .apiKey(API_KEY)
            .build();

    public static GeoApiContext context(){
        return context;
    }

}
