package com.rozzer.integration.maps.dto;

public class PlaceRequest {
    private String placeID;
    private String lat;
    private String lng;

    public PlaceRequest() {
    }

    public String getPlaceID() {
        return placeID;
    }

    public void setPlaceID(String placeID) {
        if (placeID!=null) this.placeID = "";
        else this.placeID = placeID;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

}
