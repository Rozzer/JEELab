package com.rozzer.integration.maps.service;

import com.google.common.collect.ImmutableList;
import com.google.maps.NearbySearchRequest;
import com.google.maps.PlacesApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import com.google.maps.model.PlacesSearchResponse;
import com.google.maps.model.PlacesSearchResult;
import com.rozzer.integration.maps.dto.FilterRequest;
import com.rozzer.integration.maps.dto.PlaceRequest;
import com.rozzer.integration.maps.dto.PlaceResponse;
import com.rozzer.integration.maps.utils.Filters;
import com.rozzer.model.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class MapService {
    private static final String HOST = "https://hotel-stub.herokuapp.com";
    private static final String HOTEL = "/hotel";
    private static final String GET_BY_PLACE_ID = "/placeids";
    private int radius = 50000;


    @Autowired
    private RestTemplate template;
    @Autowired
    private BookingService service;

    public FilterRequest getWithinRadios(FilterRequest filter) throws InterruptedException, ApiException, IOException {
        LatLng center = new LatLng(Double.valueOf(filter.getKvFilters().get(Filters.LATITUDE)),Double.valueOf(filter.getKvFilters().get(Filters.LONGITUDE)));

        NearbySearchRequest request = PlacesApi.nearbySearchQuery(AppContext.context(),center).radius(radius).keyword("hotel");
        if (filter.getKvFilters().containsKey(Filters.NEXT_PAGE)) {
            request.pageToken(filter.getKvFilters().get(Filters.NEXT_PAGE));
        }
        PlacesSearchResponse placeResult = request.await();

        filter.getKvFilters().put(Filters.NEXT_PAGE,placeResult.nextPageToken);

        PlacesSearchResult[] places = placeResult.results;

        List<String> ids = new ArrayList<>(places.length);
        Arrays.stream(places).filter(p->!p.placeId.isEmpty()).forEach(p->ids.add(p.placeId));

//        HttpEntity<List<String>> req = new HttpEntity<>(ids);        ?????????
        Hotel[] hotels = template.postForObject(HOST+HOTEL+GET_BY_PLACE_ID,ids,Hotel[].class);
        filter.getHotels().addAll(Arrays.asList(hotels));

        return filter;
    }

    public FilterRequest getByShowplace(FilterRequest filter) throws InterruptedException, ApiException, IOException {
        LatLng center = new LatLng(
                Double.valueOf(filter.getKvFilters().get(Filters.LATITUDE)),
                Double.valueOf(filter.getKvFilters().get(Filters.LONGITUDE))
        );

        NearbySearchRequest request = PlacesApi.nearbySearchQuery(AppContext.context(),center).radius(radius);

        filter.getFilters().forEach(request::keyword);

        PlacesSearchResponse placeResult = request.await();
        filter.getKvFilters().put(Filters.NEXT_PAGE,placeResult.nextPageToken);

        PlacesSearchResult[] places = placeResult.results;
                // next step???
        return null;
    }


    public List<PlaceResponse> getHotelsAroundPlace(PlaceRequest request) throws InterruptedException, ApiException, IOException {
        LatLng center = new LatLng(
                Double.valueOf(request.getLat()),
                Double.valueOf(request.getLng())
        );

        NearbySearchRequest searcher = PlacesApi.nearbySearchQuery(AppContext.context(),center).radius(radius).keyword("hotels");
        PlacesSearchResponse response = searcher.await();
        PlacesSearchResult[] results = response.results;

        List<PlaceResponse> placeResponses;
        if (results.length>0) {
            placeResponses = new ArrayList<>(results.length);
            Arrays.stream(results).forEach(hotel -> {
                PlaceResponse place = new PlaceResponse();
                place.setLat(String.valueOf(hotel.geometry.location.lat));
                place.setLng(String.valueOf(hotel.geometry.location.lng));
                place.setPlaceID(hotel.placeId);
                place.setTypes(Arrays.asList(hotel.types));
                placeResponses.add(place);
            });
        }
        else {placeResponses = ImmutableList.of(new PlaceResponse("Results no found"));}
        return placeResponses;
    }
}


