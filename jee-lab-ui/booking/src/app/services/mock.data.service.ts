import {City, Filter, Place} from '../models';
import { RestService } from './rest.service';

import * as places_file from "../../assets/mock.data/data/places.json";
import * as filters_file from "../../assets/mock.data/data/filters.json";
import * as cities_file from "../../assets/mock.data/data/cities.json";

//https://medium.com/@kgrvr/reading-local-json-present-in-an-angular-2-project-733bc3dda18e

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MockDataService {
  places: Place[] = [];
  filters: Filter[] = [];
  cities: City[] = [];

  constructor( public rest: RestService){


      for(var key in filters_file["default"]){
        var value = filters_file["default"][key];
        this.filters.push(new Filter(value.name, value.values));
      }
      /* for(var key in cities_file["default"]){
        var value = cities_file["default"][key];
        this.cities.push(new City(value.name, value.country));
      }

      for(var key in places_file["default"]){
        var value = places_file["default"][key];
        this.places.push(new Place(value.id, value.name, value.type)
          ._stars(value.stars)._price(value.price)._city(this.findCity(value.city))
          ._rating(value.rating)._wifi(value.wifi)._image("assets/mock.data/images/" + value.image));
      } */
    }

}

//Find: sudo lsof -i :4200
//Kill: sudo kill -9 <PID>
