import { Injectable } from '@angular/core';

@Injectable()
export class Strings {
  btn_weather_integration: string = 'Show weather';
  btn_tickets_integration: string = 'Show tickets';
  btn_maps_integration: string = 'Choose value on the map';
  btn_clear: string = 'Clear';
  btn_accept: string = 'Accept';
  date_from: string = 'From';
  date_to: string = 'To';

  table_header_photo: string = 'Photo';
  table_header_name: string = 'Name';
  table_header_price: string = 'Price';
  table_sort_price: string = 'By price';
  table_sort_stars: string = 'By stars';
  table_sort_feedbacks: string = 'By feedbacks';
  table_sort_remoteness: string = 'By remoteness from center';

  place_browse_place_type_label : string = 'What?';
  place_browse_city_label : string = 'Where?';
  place_browse_price_label : string = 'How much?';
  place_browse_wifi_label : string = 'Wi-Fi?';
  place_browse_breakfast_label : string = 'Breakfast?';
  place_browse_freeRoom_label : string = 'Is free?';
  place_browse_phone_label : string = 'Phone:';
  place_browse_url_label : string = 'Url:';
  place_browse_opening_hours_label : string = 'Works at:';
  place_browse_facilities_label : string = 'Facilities';
  place_browse_address_label : string = 'Address';



}
