import {City, Filter, Place} from '../models';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const endpoint = '/api';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable()
export class RestService {
  places: Place[] = [];
  filters: Filter[] = [];
  cities: City[] = [];

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getHotels(): Observable<any> {
    return this.http.get(endpoint + '/hotel/all').pipe(
      map( this.extractData ));
  }

  getHotelsByPage(page): Observable<any> {
    return this.http.get(endpoint + '/hotel/page?page=' + page).pipe(
          map( this.extractData ));
  }

  getCities(): Observable<any> {
      return this.http.get(endpoint + '/city/all').pipe(
            map( this.extractData ));
  }

  getHotelById(id): Observable<any> {
        return this.http.get(endpoint + '/hotel?id=' + id).pipe(
              map( this.extractData ));
  }

  getHotelsPages(): Observable<any> {
          return this.http.get(endpoint + '/hotel/totalpage').pipe(
                map( this.extractData ));
  }

  getFilters(): Observable<any> {
            return this.http.get(endpoint + '/hotel/filters').pipe(
                  map( this.extractData ));
  }

  getHotelsFiltered(filter, page): Observable<any> {
          return this.http.post(endpoint + '/hotel/filterwithtotal?page='+page,
              filter.map(function(x){ return {"name" : x[0]['name'], "values": x[1] }})).pipe(
                  map( this.extractData ));
  }
}
