import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from "@angular/core";
import { } from 'googlemaps';


const endpoint = '/api';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Origin':'https://guarded-shore-44262.herokuapp.com'
  })
};
///<reference path="<relevant path>/node_modules/@types/googlemaps/index.d.ts" />
@Injectable()
export class MapService {
  place_req:PlaceRequest[] = [];
  place_resp:PlaceResponse[] = [];

  constructor(private http: HttpClient) { }

  initMap(
    pos:google.maps.LatLng,
    gmap:google.maps.Map,
    gmapElement:any)
  {
    gmap = new google.maps.Map(
      gmapElement.nativeElement,
      {
        zoom: 8,
        center: pos,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        minZoom: 3
      });

    var userMarker = new google.maps.Marker({
      map: gmap,
      draggable: true,
      position: pos
    });

    google.maps.event.addListener(gmap,'click',function (event) {
      var resp = this.extractPlacesByUserClik(new PlaceRequest(event.placeId,event.latLng.lat(),event.latLng.lng()));
      this.createMarksByPlaces(resp,gmap)
    });
  }

  extractPlacesByUserClik(req:PlaceRequest){return this.http.post('/map/places',req,httpOptions)}

  private createMarksByPlaces(places:PlaceResponse[],gmap:google.maps.Map){
    places.filter(p=>p.error=="").forEach(place=>{
      var marker = new google.maps.Marker({
        map: gmap,
        draggable: true,
        position: new google.maps.LatLng(place.lat,place.lng)
      });
    })
  }
}

export class PlaceRequest {
  placeId:String;
  lat:number;
  lng:number;
  constructor(id: String, lat:number, lng:number){
    this.placeId = id;
    this.lat=lat;
    this.lng=lng;
  };

}

export class PlaceResponse {
  placeId:String;
  lat:number;
  lng:number;
  types:String[];
  error:String ;
  constructor(id: String, lat:number, lng:number){
    this.placeId = id;
    this.lat=lat;
    this.lng=lng;

  };

}
