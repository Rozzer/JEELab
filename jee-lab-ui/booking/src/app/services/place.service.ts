import { Injectable } from '@angular/core';
import { City, PlaceType, Place, Filter } from '../models';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  places: Place[] = [];
  cities: City[] = [];
  filters: any[] = [];
  pages: number = 1;

  constructor(public rest: RestService) {
    this.rest.getHotelsPages().subscribe((data: {}) => {
      this.pages = Number(data);
    });

    this.rest.getFilters().subscribe((data: {}) => {
      for( var key in data){
        var value = data[key];
        this.filters.push( [new Filter(value.name, value.values), [] ] );
      }
    });


    this.rest.getCities().subscribe((data: {}) => {
     for( var key in data){
       var value = data[key];
       this.cities.push( new City(value['name'], value['country']) );
     }
    });

    this.getFilteredHotels(this.filters, 1);
  }

    findParameter(source, target){
        for( var c in source ){
          if(source[c]['name'] == target)
            return source[c]['value'];
        }
    }

    findCity(name){
        for(var i=0; i<this.cities.length; i++){
          if( this.cities[i].name == name ) return this.cities[i];
        } return null;
    }

  find(id){
    for(var i=0; i<this.places.length; i++){
      if( this.places[i].id == id ) return this.places[i];
    } return null;
  }

  defineType(id){
    return PlaceType[id];
  }

  getFilteredHotels(filters, page){
      this.places = [];

      this.rest.getHotelsFiltered(filters, page).subscribe((data: {}) => {
          this.pages = data['totalpages'];
              var value = data['hotels'];
              for( var c_hotel in value){
                this.places.push( new Place( Number(value[c_hotel]['id']), value[c_hotel]['name'], 0 )
                 ._rating(value[c_hotel]['mark'])
                 ._address(value[c_hotel]['address'])
                 ._price( value[c_hotel]['cost'] )
                 ._stars( value[c_hotel]['stars'])
                 ._wifi( value[c_hotel]['wifi'])
                 ._breakfast( value[c_hotel]['hasBreakfast'] )
                 ._image("assets/mock.data/images/" + Math.floor(Math.random() * Math.floor(5)) + ".jpeg")
                 ._city(  this.cities[( Math.floor(Math.random() * Math.floor(this.cities.length)) )])
                 );
               }

      });
  return this.places;
  }
}
