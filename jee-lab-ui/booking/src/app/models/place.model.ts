import { City } from './city.model';

export enum PlaceType {
  Hotel,
  Villa,
  Hostel,
  Apartments,
  Cottage
};

export class Place{
  id: number;
  type: PlaceType;
  name: string;
  image: string;
  city: City;

  /* characteristics */
  price: number; //cost
  rating: number;
  wifi: boolean;
  breakfast: boolean;
  stars: number;
  freeRoom: number;
  phone: string;
  url: string;
  opening_hours: string;
  facilities: string;

  /* address */
  address: string;
  latitude: number;
  longitude: number;

  constructor(cId: number, cName:string, cType:number){
    this.id = cId;
    this.name = cName;
    switch (cType) {
    case 0:
        this.type = PlaceType.Hotel;
        break;
    case 1:
        this.type = PlaceType.Villa;
        break;
    case 2:
        this.type = PlaceType.Hostel;
        break;
    case 3:
        this.type = PlaceType.Apartments;
        break;
    case 4:
        this.type = PlaceType.Cottage;
        break;
    default:
        this.type = PlaceType.Hotel;
        break;
    };
  }

  _stars(cStars: number){
    this.stars = cStars;
    return this;
  }

  _price(cPrice: number){
    this.price = cPrice;
    return this;
  }

  _rating(cRating: number){
    this.rating = cRating;
    return this;
  }

  _city(cCity: City){
    this.city = cCity;
    return this;
  }

  _wifi(cWifi: boolean){
    this.wifi = cWifi;
    return this;
  }

  _image(cImage: string){
    this.image = cImage;
    return this;
  }

  _breakfast(cBreakfast: boolean){
      this.breakfast = cBreakfast;
      return this;
    }

  _freeRoom(cFreeRoom: number){
        this.freeRoom = cFreeRoom;
        return this;
      }

  _phone(cPhone: string){
         this.phone = cPhone;
         return this;
       }
    _url(cUrl: string){
             this.url = cUrl;
             return this;
           }
    _opening_hours(cOpeningHours: string){
                 this.opening_hours = cOpeningHours;
                 return this;
               }
    _facilities(cFacilities: string){
                     this.facilities = cFacilities;
                     return this;
                   }
    _address(cAddress: string){
                         this.address = cAddress;
                         return this;
                       }
    _latitude(cLatitude: number){
                             this.latitude = cLatitude;
                             return this;
                           }
    _longitude(cLongitude: number){
                                 this.longitude = cLongitude;
                                 return this;
                               }

}
