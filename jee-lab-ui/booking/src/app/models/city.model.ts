export class City{
  name: string;
  country: string;

  constructor(cName: string, cCountry: string){
    this.name = cName;
    this.country = cCountry;
  }
}
