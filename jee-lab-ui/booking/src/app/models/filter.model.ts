export class Filter{
  name: string;
  values: any[];

  constructor(cName: string, cValues: any[]){
    this.name = cName;
    this.values = cValues;
  }
}
