import {Component, OnInit, ViewChild} from '@angular/core';
import {MapService, PlaceService, Strings} from '../../services';
import {City, Place} from '../../models';
import { } from 'googlemaps';

@Component({
  selector: 'main',
  templateUrl: './main.html',
  styleUrls: ['./main.css']
})
///<reference path="<relevant path>/node_modules/@types/googlemaps/index.d.ts" />
export class MainComponent implements OnInit {
  old_country: string = '';
  current_page: number;
  filtered_places: Place[] = [];
  filtered_cities: City[] = [];
  show_map:boolean = false;

  constructor(private places: PlaceService, private str: Strings, private maps: MapService) {
    this.loadScripts();
    this.current_page = 1;
    this.filtered_places =  this.places.getFilteredHotels(
      this.places.filters, this.current_page
    );
    this.filtered_cities = places.cities;
    this.filtered_cities.sort(function (a, b) {
        return ('' + a.country).localeCompare(b.country);
    });
  }

  ifNewCountry(new_c: string){
    if( this.old_country === '' ){
      this.old_country = new_c;
      return true;
    } else if( this.old_country !== new_c ){
      this.old_country = new_c;
      return true;
    } else {
      return false;
    }
  }

  getFilteredHotels(){
    this.filtered_places = this.places.getFilteredHotels(
      this.places.filters, this.current_page
    );
  }

  ngOnInit() {
  }

//-----------------------------------------------
//   var host="https://guarded-shore-44262.herokuapp.com";
//   var findPlaces = '/map/find/places';
  gmap:google.maps.Map;
  @ViewChild('gmap') gmapElement: any;

  getUserGeoAndInitMap() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.extractPosition);
    } else {
      var center = new google.maps.LatLng(53.203336,50.145127);
      this.maps.initMap(center,this.gmap,this.gmapElement);
    }
      this.show_map = true;
    }
   extractPosition(pos) {
    var positions = new google.maps.LatLng(pos.coords.latitude,pos.coords.longitude);
    this.maps.initMap(positions,this.gmap,this.gmapElement);
  }

  loadScripts() {
    const dynamicScripts = ['https://maps.googleapis.com/maps/api/js?key=AIzaSyAbRfNJ6xXGUpESXYhq_gZVb6TdNYkhfow&libraries=geometry,places'];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = true;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }
}
