import { Component, forwardRef, ElementRef, ViewChild, Input, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Filter } from '../../models';

const noop = () => { };

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR4: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => PagingComponent),
      multi: true
};
@Component({
  selector: 'paging',
  templateUrl: './paging.component.html',
  styleUrls: ['./paging.component.css'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR4]
})
export class PagingComponent implements ControlValueAccessor , OnInit {
      @Input() pages : number;

      @ViewChild("page_chooser_btn_1") page_chooser_btn_1: ElementRef;
      @ViewChild("page_chooser_value_1") page_chooser_value_1: ElementRef;
      @ViewChild("value_chooser_1") value_chooser_1: ElementRef;

      @ViewChild("page_chooser_btn_2") page_chooser_btn_2: ElementRef;
      @ViewChild("page_chooser_value_2") page_chooser_value_2: ElementRef;
      @ViewChild("value_chooser_2") value_chooser_2: ElementRef;

      ngOnInit() { }

      private current: number;

      private onTouchedCallback: () => void = noop;
      private onChangeCallback: (_: any) => void = noop;

      get value(): any { return this.current; };
      set value(v: any) { this.current = v; }

      writeValue(value: any) {
        if (value !== this.current) {   this.current = value;  }
      }

      registerOnChange(fn: any) { this.onChangeCallback = fn; }
      registerOnTouched(fn: any) { this.onTouchedCallback = fn; }

      page_chooser_open($event){
        var page_chooser_btn;
        var page_chooser_value;

        if( (event.currentTarget as HTMLInputElement).getAttribute("action") == "1" ){
          page_chooser_btn = this.page_chooser_btn_1;
          page_chooser_value = this.page_chooser_value_1;
        } else{
          page_chooser_btn = this.page_chooser_btn_2;
          page_chooser_value = this.page_chooser_value_2;
        }

        page_chooser_btn.nativeElement.style.setProperty("display", "none");
        page_chooser_value.nativeElement.style.setProperty("display", "block");
      }

      page_chooser_set($event){
          var page_chooser_btn;
          var page_chooser_value;
          var value_chooser;

          if( (event.currentTarget as HTMLInputElement).getAttribute("action") == "1" ){
            page_chooser_btn = this.page_chooser_btn_1;
            page_chooser_value = this.page_chooser_value_1;
            value_chooser = this.value_chooser_1;
          } else{
            page_chooser_btn = this.page_chooser_btn_2;
            page_chooser_value = this.page_chooser_value_2;
            value_chooser = this.value_chooser_2;
          }

          page_chooser_btn.nativeElement.style.setProperty("display", "block");
          page_chooser_value.nativeElement.style.setProperty("display", "none");
          this.onChange(event, value_chooser.nativeElement.value)
      }

      page_previous(){
          var pg = this.current > 0 ? this.current-1 : this.current;
          this.onChange(0, pg);
      }

       page_next(){
           var pg = this.current < this.pages ? this.current+1 : this.current;
           this.onChange(0, pg);
       }

      onChange($event, v) {
          if( v > 0 && v <= this.pages ){
            this.current = Number(v);
            this.onChangeCallback(this.current);
          }
      }
}
