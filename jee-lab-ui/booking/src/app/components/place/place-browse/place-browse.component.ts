import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { PlaceService , Strings } from '../../../services';
import { Place } from '../../../models';

@Component({
  selector: 'place-browse',
  templateUrl: './place-browse.html',
  styles: [ ` .header,.content{ margin: 15px; }
              rating { margin: -5px 20px 20px 20px; }
              .header > h3 { margin: 0px 20px; }
              img{ width: 200px; height: 200px; }
     ` ]
})
export class PlaceBrowseComponent implements OnInit {

  place: Place ;

  constructor(private route: ActivatedRoute, private ps: PlaceService, private str: Strings) {
    var id;
    this.route.params.subscribe( params => id = params.id );
    this.place = ps.find(id);
  }

  ngOnInit() {
  }

}
