import { Component, forwardRef, ElementRef, ViewChild, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Place } from '../../../models';
import { Strings } from '../../../services';

const noop = () => { };

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR_2: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => PlaceListComponent),
      multi: true
};
@Component({
  selector: 'place-list',
  templateUrl: './place-list.html',
  styleUrls: ['./place-list.css'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR_2]
})
export class PlaceListComponent implements ControlValueAccessor , OnInit {
      @Input() places : Place[] = [];

      constructor(private router: Router, private str: Strings){
      }

      ngOnInit() {
      }

      private innerValue: any = [];

      private onTouchedCallback: () => void = noop;
      private onChangeCallback: (_: any) => void = noop;

      get value(): any {
        return this.innerValue;
      };

      set value(v: any) {
          this.innerValue = v;
      }

      writeValue(value: any) {
        if (value !== this.innerValue) {
          this.innerValue = value;
        }
      }

      registerOnChange(fn: any) { this.onChangeCallback = fn; }

      registerOnTouched(fn: any) { this.onTouchedCallback = fn; }

      onChange($event, v) {
          this.innerValue = v;
          this.onChangeCallback(v);
      }

      openPlace(a){
        this.router.navigate(['/place', a.id ]);
        // or redirect to booking site
      }

      sortByStars(path /* >0 incr, <0 decr, 0 skip */){
        if( path > 0 ){
          this.places.sort((a,b) => (a.stars > b.stars) ? 1 : ((b.stars > a.stars) ? -1 : 0));
        } else if( path < 0 ) {
          this.places.sort((a,b) => (a.stars > b.stars) ? -1 : ((b.stars > a.stars) ? 1 : 0));
        } else {

        }
      }

      sortByPrice(path /* >0 incr, <0 decr, 0 skip */){
        if( path > 0 ){
          this.places.sort((a,b) => (a.price > b.price) ? 1 : ((b.price > a.price) ? -1 : 0));
        } else if( path < 0 ) {
          this.places.sort((a,b) => (a.price > b.price) ? -1 : ((b.price > a.price) ? 1 : 0));
        } else {

        }
      }

      sortByFeedbacks(path /* >0 incr, <0 decr, 0 skip */){
        if( path > 0 ){
          this.places.sort((a,b) => (a.rating > b.rating) ? 1 : ((b.rating > a.rating) ? -1 : 0));
        } else if( path < 0 ) {
          this.places.sort((a,b) => (a.rating > b.rating) ? -1 : ((b.rating > a.rating) ? 1 : 0));
        } else {

        }
      }

      sortByRemoteness(path /* >0 incr, <0 decr, 0 skip */){
        // TODO - need map integration
      }
}
