import { Component, forwardRef, ElementRef, ViewChild, Input, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Filter } from '../../models';

const noop = () => { };
/*
  https://embed.plnkr.co/nqKUSPWb6w5QXr8a0wEu/?show=preview
  https://almerosteyn.com/2016/04/linkup-custom-control-to-ngcontrol-ngmodel
  https://medium.com/@balramchavan/smarter-way-to-organize-import-statements-using-index-ts-file-s-in-angular-c685e9d645b7
*/
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FilterComponent),
      multi: true
};
@Component({
  selector: 'filter',
  template: `<h5> {{ filter.name }} </h5>
    <div *ngFor="let n of filter.values; let i = index" class="checkbox">
      <label><input type="checkbox" value="n" (change)="onChange($event, n)">{{n}}</label>
    </div>`,
  styles: [ 'label, input { margin: 0px 5px; padding: 0px 5px; }' ],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class FilterComponent implements ControlValueAccessor , OnInit {
      @Input() filter : Filter;
      checked: any[] = [];

      ngOnInit() {
      }

        //The internal data model
      private innerValue: any = [];

        //Placeholders for the callbacks which are later provided by the Control Value Accessor
      private onTouchedCallback: () => void = noop;
      private onChangeCallback: (_: any) => void = noop;

        //get accessor
      get value(): any {
        return this.innerValue;
      };

        //set accessor including call the onchange callback
      set value(v: any) {
          this.innerValue = v;
      }

        //From ControlValueAccessor interface
      writeValue(value: any) {
        if (value !== this.innerValue) {
          this.innerValue = value;
        }
      }

        //From ControlValueAccessor interface
      registerOnChange(fn: any) { this.onChangeCallback = fn; }

        //From ControlValueAccessor interface
      registerOnTouched(fn: any) { this.onTouchedCallback = fn; }

      onChange($event, v) {
          if($event.target.checked){
            this.checked.push(v);
          }else{
            var index = this.checked.indexOf(v);
            if (index !== -1) this.checked.splice(index, 1);
          }
          this.innerValue = this.checked;
          this.onChangeCallback(this.innerValue);
      }
}









