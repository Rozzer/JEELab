import { Component } from '@angular/core';
import { Filter } from './models';
@Component({
  selector: 'app-root',
  template: `
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link active" href="https://ssau.ru/">Samara Univercity</a>
      </li>
    </ul>
  <div style="height:80%;">
    <router-outlet></router-outlet>
  </div>
  `,
  styles: [ ]
})
export class AppComponent  {

}
