import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaceListComponent, FilterComponent, MainComponent, PlaceBrowseComponent,
  PageNotFoundComponent, RatingComponent, PagingComponent } from './components';
import { Strings, RestService, MapService } from './services';

@NgModule({
  declarations: [
    AppComponent, FilterComponent, PlaceListComponent, MainComponent, PageNotFoundComponent, PlaceBrowseComponent, RatingComponent, PagingComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule, FormsModule, HttpClientModule, CommonModule
  ],
  providers: [ Strings, RestService, MapService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
