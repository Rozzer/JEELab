import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {MessageService} from './message.service';
import {Observable, of} from 'rxjs';
import {Author} from "../model/author";

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  private url = 'author';

  constructor(private http: HttpClient,
              private messageService: MessageService) {
  }


  /** GET all author */
  getAuthors (): Observable<Author[]> {
    return this.http.get<Author[]>(this.url+"/all")
      .pipe(
        tap(authors => this.log('fetched author')),
        catchError(this.handleError('getAuthors', []))
      );
  }

  /** GET author by id.  */
  getAuthor(id: number): Observable<Author> {
    const url = `${this.url}/${id}`;
    return this.http.get<Author>(url).pipe(
      tap(_ => this.log(`fetched author id=${id}`)),
      catchError(this.handleError<Author>(`getAuthor id=${id}`))
    );
  }


  /** Write messages as logs */
  private log(message: string) {
    this.messageService.add(`AuthorService: ${message}`);
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
