import {Component, Input, OnInit} from '@angular/core';
import {Book} from "../model/book";
import {GenreService} from "../services/genre.service";
import {BookService} from "../services/book.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AuthorService} from "../services/author.service";
import {Author} from "../model/author";

@Component({
  selector: 'book-editor',
  styleUrls: ['./book.editor.css'],
  templateUrl: './book.editor.component.html',
})
export class BookEditorComponent implements OnInit {

  genres: String[];
  authors: Author[];

  @Input() book: Book;


  constructor(genreService: GenreService, private bookService: BookService,
              authorService: AuthorService, private modalService: NgbModal) {
    genreService.getGenres().subscribe((value : String[]) => {
      this.genres = value;
    });
    authorService.getAuthors().subscribe((value : Author[]) =>{
      this.authors = value;
    })
  }

  ngOnInit() {
  }


  open(content){
    if (this.book ==null){
      this.book= new Book();
    }
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.book = result;
      this.save();
    }, (reason) => {
      this.book = reason;
    });
  }

  save(){
    if (this.book.id == null){
      this.bookService.postBook(this.book).subscribe();
    } else {
      this.bookService.putBook(this.book).subscribe();
    }
  }



}
